package com.clientmanager.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name="employee")
public class Employee extends BaseModel {

    @Column(name="firstname") private String firstname;
    @Column(name="lastname") private String lastname;
    @Column(name="intials") private String initials;
    @Column(name = "title") private String title;
    @Column(name = "preferred_name") private String preferredName;
    @Column(name = "disability") private String disability;
    @Column(name = "gender") private String gender;
    @Column(name = "race") private String race;
    @Column(name = "identity_type") private String identityType;
    @Column(name = "id_passport_no") private String IdPassportno;
    @Column(name = "country") private String country;
    @Column(name = "date_of_birth") private LocalDate dob;
    @Column(name = "employee_number") private String employeeNo;
    @Column(name = "nature_of_person") private String natureOfPerson;

    @Column(name = "payment_method") private String paymentMethod;
    @Column(name = "job_title") private String jobTitle;
    @Column(name = "start_date") private LocalDate startDate;
    @Column(name = "tax_status") private String taxStatus;
    @Column(name = "tax_ref_no") private String taxReferenceNumber;
    @Column(name = "uif_exempt") private String uifExempt;
    @Column(name = "uif_exempt_reason") private String uifExemptReason;
    @Column(name = "own_medical") private Boolean paysOwnMedicalAid;
    @Column(name = "no_dependents") private int NoOfDependents;
    @Column(name = "medical_aid_name") private String medicalAidName;
    @Column(name = "medical_aid_number") private String medicalNumber;

    @Column(name = "company_id")
    @JoinColumn(foreignKey = @ForeignKey(name = "id"), table = "company", updatable = false)
    private UUID companyId;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name="join_employee_address",
            joinColumns = @JoinColumn( name="entity_id"),
            inverseJoinColumns = @JoinColumn( name="address_id"))
    private List<Address> address;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name="join_employee_contact",
            joinColumns = @JoinColumn( name="entity_id"),
            inverseJoinColumns = @JoinColumn( name="contact_id"))
    private List<ContactDetails> contactDetails;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name="join_company_login_details",
            joinColumns = @JoinColumn( name="entity_id"),
            inverseJoinColumns = @JoinColumn( name="login_id"))
    private List<LoginDetails> loginDetails;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bank_details_id")
    private BankingDetails bankingDetails;


    public Employee() {
    }

    public Employee(String firstname, String lastname, String initials, String title, String preferredName, String disability,
                    String gender, String race, String identityType, String idPassportno, String country, LocalDate dob,
                    String employeeNo, String natureOfPerson, String paymentMethod, String jobTitle, LocalDate startDate,
                    String taxStatus, String taxReferenceNumber, String uifExempt, String uifExemptReason, Boolean paysOwnMedicalAid,
                    int noOfDependents, String medicalAidName, String medicalNumber, UUID companyId, List<Address> address,
                    List<ContactDetails> contactDetails, List<LoginDetails> loginDetails, BankingDetails bankingDetails) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.initials = initials;
        this.title = title;
        this.preferredName = preferredName;
        this.disability = disability;
        this.gender = gender;
        this.race = race;
        this.identityType = identityType;
        this.IdPassportno = idPassportno;
        this.country = country;
        this.dob = dob;
        this.employeeNo = employeeNo;
        this.natureOfPerson = natureOfPerson;
        this.paymentMethod = paymentMethod;
        this.jobTitle = jobTitle;
        this.startDate = startDate;
        this.taxStatus = taxStatus;
        this.taxReferenceNumber = taxReferenceNumber;
        this.uifExempt = uifExempt;
        this.uifExemptReason = uifExemptReason;
        this.paysOwnMedicalAid = paysOwnMedicalAid;
        this.NoOfDependents = noOfDependents;
        this.medicalAidName = medicalAidName;
        this.medicalNumber = medicalNumber;
        this.companyId = companyId;
        this.address = address;
        this.contactDetails = contactDetails;
        this.loginDetails = loginDetails;
        this.bankingDetails = bankingDetails;
    }

    public UUID getCompanyId() {
        return companyId;
    }

    public void setCompanyId(UUID companyId) {
        this.companyId = companyId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPreferredName() {
        return preferredName;
    }

    public void setPreferredName(String preferredName) {
        this.preferredName = preferredName;
    }

    public String getDisability() {
        return disability;
    }

    public void setDisability(String disability) {
        this.disability = disability;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdPassportno() {
        return IdPassportno;
    }

    public void setIdPassportno(String idPassportno) {
        IdPassportno = idPassportno;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getEmployeeNo() {
        return employeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getNatureOfPerson() {
        return natureOfPerson;
    }

    public void setNatureOfPerson(String natureOfPerson) {
        this.natureOfPerson = natureOfPerson;
    }

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    public List<ContactDetails> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetails> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public List<LoginDetails> getLoginDetails() {
        return loginDetails;
    }

    public void setLoginDetails(List<LoginDetails> loginDetails) {
        this.loginDetails = loginDetails;
    }

    public BankingDetails getBankingDetails() {
        return bankingDetails;
    }

    public void setBankingDetails(BankingDetails bankingDetails) {
        this.bankingDetails = bankingDetails;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getTaxStatus() {
        return taxStatus;
    }

    public void setTaxStatus(String taxStatus) {
        this.taxStatus = taxStatus;
    }

    public String getTaxReferenceNumber() {
        return taxReferenceNumber;
    }

    public void setTaxReferenceNumber(String taxReferenceNumber) {
        this.taxReferenceNumber = taxReferenceNumber;
    }

    public String getUifExempt() {
        return uifExempt;
    }

    public void setUifExempt(String uifExempt) {
        this.uifExempt = uifExempt;
    }

    public String getUifExemptReason() {
        return uifExemptReason;
    }

    public void setUifExemptReason(String uifExemptReason) {
        this.uifExemptReason = uifExemptReason;
    }

    public Boolean getPaysOwnMedicalAid() {
        return paysOwnMedicalAid;
    }

    public void setPaysOwnMedicalAid(Boolean paysOwnMedicalAid) {
        this.paysOwnMedicalAid = paysOwnMedicalAid;
    }

    public int getNoOfDependents() {
        return NoOfDependents;
    }

    public void setNoOfDependents(int noOfDependents) {
        NoOfDependents = noOfDependents;
    }

    public String getMedicalAidName() {
        return medicalAidName;
    }

    public void setMedicalAidName(String medicalAidName) {
        this.medicalAidName = medicalAidName;
    }

    public String getMedicalNumber() {
        return medicalNumber;
    }

    public void setMedicalNumber(String medicalNumber) {
        this.medicalNumber = medicalNumber;
    }

}
