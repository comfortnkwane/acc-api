package com.clientmanager.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "contact")
public class ContactDetails extends BaseModel {

    @Column(name="contact") private String contact;
    @Column(name="contact_type") private String contactType;

    public ContactDetails() {
    }

    public ContactDetails(String contact, String contactType) {
        this.contact = contact;
        this.contactType = contactType;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }
}
