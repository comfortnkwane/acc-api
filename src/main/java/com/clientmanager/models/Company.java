package com.clientmanager.models;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name ="company")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Company extends BaseModel {

    @Column(name="company_name") private String companyName;
    @Column(name="trade_name") private String tradingName;
    @Column(name="registration_number") private String registrationNumber;
    @Column(name="registration_date") private LocalDate RegistrationDate;
    @Column(name="business_start_date") private String businessStartDate;
    @Column(name="company_type") private String companyType;
    @Column(name="financial_year_end") private LocalDate financialYearEnd;
    @Column(name="tax_number") private String taxNumber;
    @Column(name="vat_number") private String vatNumber;
    @Column(name="paye_number") private String payeNumber;
    @Column(name="dol_uif_number") private String dolUifNumber;
    @Column(name="contact_person") private String contactPerson;
    @Column(name = "bbbee_level") private String bbbeeLevel;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name="join_company_address",
            joinColumns = @JoinColumn( name="entity_id"),
            inverseJoinColumns = @JoinColumn( name="address_id"))
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Address> address;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name="join_company_contact",
            joinColumns = @JoinColumn( name="entity_id"),
            inverseJoinColumns = @JoinColumn( name="contact_id"))
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ContactDetails> contactDetails;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name="join_company_login_details",
            joinColumns = @JoinColumn( name="entity_id"),
            inverseJoinColumns = @JoinColumn( name="login_id"))
    @Fetch(value = FetchMode.SUBSELECT)
    private List<LoginDetails> loginDetails;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "bank_details_id")
    private BankingDetails bankingDetails;


    public Company() {
    }

    public Company(String companyName, String tradingName, String registrationNumber, LocalDate registrationDate,
                   String businessStartDate, String companyType, LocalDate financialYearEnd, String taxNumber, String vatNumber,
                   String payeNumber, String dolUifNumber, String contactPerson, String bbbeeLevel, List<Address> address,
                   List<ContactDetails> contactDetails, List<LoginDetails> loginDetails, BankingDetails bankingDetails) {
        this.companyName = companyName;
        this.tradingName = tradingName;
        this.registrationNumber = registrationNumber;
        RegistrationDate = registrationDate;
        this.businessStartDate = businessStartDate;
        this.companyType = companyType;
        this.financialYearEnd = financialYearEnd;
        this.taxNumber = taxNumber;
        this.vatNumber = vatNumber;
        this.payeNumber = payeNumber;
        this.dolUifNumber = dolUifNumber;
        this.contactPerson = contactPerson;
        this.bbbeeLevel = bbbeeLevel;
        this.address = address;
        this.contactDetails = contactDetails;
        this.loginDetails = loginDetails;
        this.bankingDetails = bankingDetails;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public LocalDate getRegistrationDate() {
        return RegistrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        RegistrationDate = registrationDate;
    }

    public String getBusinessStartDate() {
        return businessStartDate;
    }

    public void setBusinessStartDate(String businessStartDate) {
        this.businessStartDate = businessStartDate;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public LocalDate getFinancialYearEnd() {
        return financialYearEnd;
    }

    public void setFinancialYearEnd(LocalDate financialYearEnd) {
        this.financialYearEnd = financialYearEnd;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getPayeNumber() {
        return payeNumber;
    }

    public void setPayeNumber(String payeNumber) {
        this.payeNumber = payeNumber;
    }

    public String getDolUifNumber() {
        return dolUifNumber;
    }

    public void setDolUifNumber(String dolUifNumber) {
        this.dolUifNumber = dolUifNumber;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getBbbeeLevel() {
        return bbbeeLevel;
    }

    public void setBbbeeLevel(String bbbeeLevel) {
        this.bbbeeLevel = bbbeeLevel;
    }

    public List<LoginDetails> getLoginDetails() {
        return loginDetails;
    }

    public void setLoginDetails(List<LoginDetails> loginDetails) {
        this.loginDetails = loginDetails;
    }

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    public List<ContactDetails> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetails> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public BankingDetails getBankingDetails() {
        return bankingDetails;
    }

    public void setBankingDetails(BankingDetails bankingDetails) {
        this.bankingDetails = bankingDetails;
    }
}
