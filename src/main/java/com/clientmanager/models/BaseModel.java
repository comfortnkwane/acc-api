package com.clientmanager.models;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@MappedSuperclass
public class BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected UUID id;

    protected BaseModel(UUID id) {
        this.id = id;
    }

    protected BaseModel() {}

    public UUID getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseModel baseEntity = (BaseModel) o;

        return id.equals(baseEntity.id);
    }

    @Override
    public int hashCode() {
        if(null == id)
            return 0;
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "BaseModel{" + "id=" + id +
                '}';
    }

}
