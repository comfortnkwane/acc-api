package com.clientmanager.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "tbl_banking")
@Entity
public class BankingDetails extends BaseModel {

    @Column(name = "bank_name") private String bankName;
    @Column(name = "account_type") private String accountType;
    @Column(name = "account_number") private String accountNumber;
    @Column(name = "branch_name") private String branch;
    @Column(name="branch_code") private String branchCode;
    @Column(name="account_holder") private String accountHolder;
    @Column(name = "account_relationship") private String accountRelationship;

    public BankingDetails() {
    }

    public BankingDetails(String bankName, String accountType, String accountNumber, String branch, String branchCode,
                          String accountHolder, String accountRelationship) {
        this.bankName = bankName;
        this.accountType = accountType;
        this.accountNumber = accountNumber;
        this.branch = branch;
        this.branchCode = branchCode;
        this.accountHolder = accountHolder;
        this.accountRelationship = accountRelationship;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getAccountHolder() {
        return accountHolder;
    }

    public void setAccountHolder(String accountHolder) {
        this.accountHolder = accountHolder;
    }

    public String getAccountRelationship() {
        return accountRelationship;
    }

    public void setAccountRelationship(String accountRelationship) {
        this.accountRelationship = accountRelationship;
    }

}
