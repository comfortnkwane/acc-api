package com.clientmanager.models;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="login_details")
public class LoginDetails extends BaseModel {

    @Column(name = "username")
    private String username;
    @Column(name="password")
    private String password;
    @Column(name="type")
    private String type;

    public LoginDetails() { }

    public LoginDetails(String username, String password, String type) {
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
