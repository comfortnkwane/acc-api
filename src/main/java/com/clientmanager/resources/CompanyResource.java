package com.clientmanager.resources;

import com.clientmanager.models.Company;
import com.clientmanager.repos.CompanyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping(value = "/company")
public class CompanyResource {

    private CompanyRepo companyRepo;

    @Autowired
    private CompanyResource(CompanyRepo companyRepo){
        this.companyRepo = companyRepo;
    }

    @GetMapping()
    public List<Company> getAll(){
        return companyRepo.findAll();
    }

    @PostMapping("/create")
    public Company companyCreate(@RequestBody Company company){
        return companyRepo.save(company);
    }

    @PutMapping("/update")
    public Company companyUpdate(@RequestBody Company company){
        return companyRepo.save(company);
    }

    @GetMapping("/{id}")
    public Company getCompany(@PathVariable final UUID id){
        return companyRepo.getOne(id);
    }
}
