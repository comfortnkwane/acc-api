package com.clientmanager.resources;
import com.clientmanager.models.Employee;
import com.clientmanager.repos.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping(value = "/employee")
public class EmployeeResource {

    private EmployeeRepo EmployeeRepo;

    @Autowired
    private EmployeeResource(EmployeeRepo EmployeeRepo){
        this.EmployeeRepo = EmployeeRepo;
    }

    @GetMapping()
    public List<Employee> getAll(){
        return EmployeeRepo.findAll();
    }

    @PostMapping("/create")
    public Employee EmployeeCreate(@RequestBody Employee Employee){
        return EmployeeRepo.save(Employee);
    }

    @GetMapping("/:id")
    public Employee getEmployee(@PathVariable final UUID EmployeeId){
        return EmployeeRepo.getOne(EmployeeId);
    }
}
